var pictureSource;   // picture source
var destinationType; // sets the format of returned value

document.addEventListener("deviceready",onDeviceReady,false);

function onDeviceReady() {
    pictureSource=navigator.camera.PictureSourceType;
    destinationType=navigator.camera.DestinationType;
}

function onPhotoDataSuccess(imageURI) {

    var smallImage = document.getElementById('smallImage');
    smallImage.style.display = 'block';
    smallImage.src = imageURI;
    moveFile(imageURI);
}

function onPhotoURISuccess(imageURI) {

    var largeImage = document.getElementById('largeImage');
    largeImage.style.display = 'block';
    largeImage.src = imageURI;
}

function capturePhoto() {
    // Take picture using device camera and retrieve image 
    navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
    destinationType: destinationType.FILE_URI,
    saveToPhotoAlbum: true});
}

function getPhoto(source) {

    navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
        destinationType: destinationType.FILE_URI,
        sourceType: source });
}

function onFail(message) {
    alert('Failed because: ' + message);
}

/*****************salvataggio*/

function movePic(file){
    window.resolveLocalFileSystemURI(file, resolveOnSuccess, resOnError);
}

//Callback function when the file system uri has been resolved
function resolveOnSuccess(entry){
    var d = new Date();
    var n = d.getTime();
    //new file name
    var newFileName = n + ".jpg";
    var myFolderApp = "Geofolder";

    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys) {
            //The folder is created if doesn't exist
        var direct = fileSys.root;
        alert(direct);
            direct.getDirectory( myFolderApp,
                {create:true, exclusive: false},
                function(myFolderApp) {
                    entry.moveTo(myFolderApp, newFileName,  successMove, resOnError);
                },
                resOnError);
        },
        resOnError);
}

//Callback function when the file has been moved successfully - inserting the complete path
function successMove(entry) {
    //Store imagepath in session for future use
    // like to store it in database
    sessionStorage.setItem('imagepath', entry.fullPath);

}

function resOnError(error) {
    alert(error.code);
}

function moveFile(fileUri) {
    window.resolveLocalFileSystemURL(
        fileUri,
        function(fileEntry){
            window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory,
                function(dirEntry) {
                    fileEntry.moveTo(dirEntry, "fileName.jpg", function(entry){
                        alert("File moved.check internal memory");
                    },resOnError);
                },
                resOnError);
        },
        resOnError);}