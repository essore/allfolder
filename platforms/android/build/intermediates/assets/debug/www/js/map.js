
var def_lat=43.58;
var def_long=13.51;
var def_accur=300;
var timer=0;

var longit = localStorage.Longitude;
var latit = localStorage.Latitude;
var precisione = localStorage.Accuracy;
if(localStorage.Timer >=0 ){
    timer=localStorage.Timer;
}

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
        navigator.geolocation.getCurrentPosition(pos_onSuccess, pos_onError,{timeout:2000});
}

function pos_onSuccess(position) {

    localStorage.Latitude=position.coords.latitude;
    localStorage.Longitude=position.coords.longitude;
    localStorage.Accuracy=position.coords.accuracy;
    localStorage.Timer=Date.now()/1000;

    longit = localStorage.Longitude;
    latit = localStorage.Latitude;
    precisione = localStorage.Accuracy;
    timer=localStorage.Timer;

    var element = document.getElementById('attuale');
    element.innerHTML =
        'posizione attuale <br> Lat: '+ latit+ ' Long: '+ longit + ' Acc: '+ precisione;

    setMappa(longit,latit,precisione);
}

// onError Callback receives a PositionError object
//
function pos_onError(error) {

    var now = Date.now()/1000;         //timestamp in secondi
    if(now - timer > 900){         //900 secondi sono 15 minuti
        //se l'ultima posizione è più vecchia di 15 minuti uso la posizione default
        Latit=def_lat;
        Longit=def_long;
        Precisione=def_accur;
        var element = document.getElementById('attuale');
        element.innerHTML = 'posizione default <br>' +
            'Lat: '+ latit+ ' Long: '+ longit + ' Acc: '+ precisione;
    }else {
        //altrimenti uso l'ultima posizione salvata sul local storage
        longit = localStorage.Longitude;
        latit = localStorage.Latitude;
        precisione = localStorage.Accuracy;
        var element = document.getElementById('attuale');
        element.innerHTML = 'posizione di poco fa <br>' +
            'Lat: '+ latit+ ' Long: '+ longit + ' Acc: '+ precisione;
    }

    setMappa(longit,latit,precisione);
}

/*eros: test: inserisce nell'html i dati relativi alla posione attuale*/


function setMappa(longit,latit,precisione) {
    /* importa e setta l'oggetto mappa*/
    var zoom = 13;
    if (precisione < 25) zoom = 18;
        else if (precisione < 50) zoom = 17;
        else if (precisione < 100) zoom = 16;
        else if (precisione < 200) zoom = 15;

    var map = L.map('mappa').setView([latit, longit], zoom);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'cucx.01lnc4la',
        accessToken: 'pk.eyJ1IjoiY3VjeCIsImEiOiJjaW5zeWVwcTgwMGtvdmZtMnd5enNtdmQyIn0.IpIqLtNxWb3LAjODkHpgow'
    }).addTo(map);

    /*aggiungo un cerchio*/
    L.circle([latit, longit], precisione, {
        color: 'red',
        fillColor: 'blue',
        fillOpacity: 0.3
    }).addTo(map);


    /* merker con contenuto */
    L.marker([43.6, 13.5]).addTo(map).bindPopup("<img width='300px' src='img/idea.png' />");
    L.marker([43.59, 13.51]).addTo(map).bindPopup("<i>Someone else</i>");
    L.marker([43.58, 13.52]).addTo(map).bindPopup("<i>Someone else</i>");
    L.marker([43.57, 13.53]).addTo(map).bindPopup("<i>Someone else</i>");
}

//aspetto 10 secondi poi....
setTimeout(function(){
    setInterval(fiveSecondFunction, 5000);
},10*1000);
//....aggiorno la mappa ogni 5 secondi
function fiveSecondFunction() {
    navigator.geolocation.getCurrentPosition(pos_onSuccess, pos_onError,{timeout:2000});
}
